# RedArrow project front-end 

#Установка (как отдельного репозитария)

Необходимо иметь node, npm, gulp, sass

sass:

```
gem install sass
```

gulp:

```npm
npm install --global gulp-cli
```

Для разработки необходимо установить npm пакеты

```npm
npm install
```

#Таски галпа:

Для запуска сервера

```npm
gulp watch

```

Для билда проекта в отдельную папку build

```npm
gulp build

```

Для удаления папки build

```npm
gulp clean

```

Чтобы отдельно сгенерить css из sass без вотчера

```npm
gulp sass

```

Чтобы сгенерить спрайт из иконок в папке img/icons. Генерация будет только из самой папки, без поддиректорий.

```npm
gulp sprite

```

Чтобы сгенерить спрайт из иконок в папке, задаём путь относительно icons. 
Например: gulp --folder=options sprite
(src/img/icons/options)

```npm
gulp --folder=Папка sprite
```
