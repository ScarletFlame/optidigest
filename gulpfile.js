/**
 * GULP 4.x
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var browsersync = require("browser-sync").create();
var del = require('del');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var svgSprite = require('gulp-svg-sprite');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var args = require('minimist')(process.argv.slice(2));
var rename = require("gulp-rename");
var htmlmin = require("gulp-htmlmin");
var concat = require('gulp-concat');

//var sourcemaps = require('gulp-sourcemaps');
//var cssbeautify = require('gulp-cssbeautify');
//var gutil = require('gulp-util'); // deprecated

// === Featured... ===
// var eslint = require("gulp-eslint");
// var imagemin = require("gulp-imagemin");
// var postcss = require("gulp-postcss");
// var cssnano = require("cssnano");

// Build (destination) directory path
var app = {
  build: 'build'
};

// Source and destionation assets paths
var paths = {
  styles: {
    src: 'src/scss/**/*.scss',
    dest: app.build + '/css/'
  },
  scripts: {
    src: 'src/js/**/*.js',
    dest: app.build + '/js/',
  },
  libs: {
    src: 'src/libs/**/*',
    dest: app.build + '/libs',
  },
  images: {
    src: 'src/img/**/*',
    dest: app.build + '/img'
  },
  fonts: {
    src: 'src/fonts/**/*',
    dest: app.build + '/fonts'
  },
  html: {
    src: 'src/*.html',
    dest: app.build
  },
  sprites: {
    src: 'src/icons',
    dest: 'src/img/sprites/'
  },
};

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./" + app.build + "/"
    },
    port: 3000
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

/**
 * gulp clean
 *
 * Delete "build" directory
 */
function clean() {
  return del([ app.build ]);
}

/**
 * gulp clean_sprites
 *
 * Delete directory with sprites "src/img/sprites"
 * You need to generate new by hands!
 */
function clean_sprites() {
  return del([ paths.sprites.dest ]);
}

/**
 * gulp styles
 *
 */
function styles() {
  // https://github.com/jakubpawlowicz/clean-css
  var cssOptions = {
    format: 'beautify', // keep line breaks
    compatibility: '*', // (default) - Internet Explorer 10+ compatibility mode
  };

  return gulp
    .src(paths.styles.src)
    .pipe(plumber())
    .pipe(sass())
    .pipe(cleanCSS(cssOptions))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    //.pipe(concat('build.css'))
    .pipe(gulp.dest(paths.styles.dest));
}

// Lint scripts
// function scriptsLint() {
//   return gulp
//     .src(["./assets/js/**/*", "./gulpfile.js"])
//     .pipe(plumber())
//     .pipe(eslint())
//     .pipe(eslint.format())
//     .pipe(eslint.failAfterError());
// }

/**
 * gulp scripts
 *
 *  — Uglify cannot parse ES6, only after babel
 */
function scripts() {
  return gulp
    .src(paths.scripts.src, { sourcemaps: false })
    .pipe(plumber())
    .pipe(gulp.dest(paths.scripts.dest));
}

/**
 * gulp images
 *
 * Just copy img folder to the build/dist image folder.
 * Add imagemin (gulp-imagemin) or something like this
 */
function images() {
  return gulp
    .src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest))
}

/**
 * gulp fonts
 *
 * Just copy img folder to the build/dist folder.
 */
function fonts() {
  return gulp
    .src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest))
}

/**
 * gulp html
 *
 * Process HTML to the build/dist folder.
 */
function html() {
  return gulp
    .src(paths.html.src)
    //.pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(paths.html.dest))
}

/**
 * gulp libs
 *
 * Process JS Libs to the build/dist folder.
 */
function libs() {
  return gulp
    .src(paths.libs.src)
    .pipe(gulp.dest(paths.libs.dest))
}

/**
 *  gulp sprites --folder=name
 *
 *  Generate sprite from (relative!) folder and save it inside src (not to the build)
 *
 *  Examples: gulp --folder=social
 *
 *  Make sprite from all images from src/img/icons/social
 *  Result file will be: src/img/sprites/sprite.social.svg
 *
 *  If there is no --folder option, it will create "sprite.icons.svg" from the root of the "icons" directory
 */
function sprites() {
  return gulp
    .src('*.svg', {cwd: paths.sprites.src + (args['folder'] ? '/' + args['folder'] : '')})
    .pipe(svgSprite({
      mode: {
        symbol: true,
      }
    }))
    .pipe(rename(function(path) {
      path.dirname = '';
      if (args['folder']) {
        path.basename = 'sprite.' + args['folder'];
      }
      else {
        path.basename = 'sprite.' + 'icons';
      }
      return path;
    }))
    .pipe(gulp.dest(paths.sprites.dest));
}

/**
 * gulp watchFiles
 *
 * HTML, SCSS and JS (without sprites and images)
 */
function watchFiles() {
  gulp.watch(paths.styles.src, gulp.series(styles, browserSyncReload));
  gulp.watch(paths.scripts.src, gulp.series(scripts, browserSyncReload));
  gulp.watch(paths.html.src, gulp.series(images, browserSyncReload));
  gulp.watch(paths.html.src, gulp.series(html, browserSyncReload));
}

/**
 * gulp build
 */
var build = gulp.series(clean, gulp.parallel(styles, scripts, images, fonts, html, libs));

/**
 * gulp watch
 */
var watch = gulp.parallel(watchFiles, browserSync);

/**
 * gulp js
 */
//var js = gulp.series(scriptsLint, scripts);

/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.clean = clean;
exports.clean_sprites = clean_sprites;
exports.styles = styles;
//exports.js = js;
exports.scripts = scripts;
exports.libs = libs;
exports.sprites = sprites;
exports.images = images;
exports.fonts = fonts;
exports.html = html;
//exports.serve = serve;
exports.watch = watch;
exports.build = build;
/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = build;