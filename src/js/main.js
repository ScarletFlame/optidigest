$(function () {
  const $solution = $('.od-solution__solution');
  $('.od-solution__banner').on('click', function() {
    $solution.addClass('od-solution__solution_opened').one('transitionend', function () {
      $solution.addClass('od-solution__solution_fixed');
      $('.od-solution__to-hide').remove();
    });

  });

  $('a[data-scroll]').on('click', function() {
    const $scroll = $('div[data-name="' + $(this).data('scroll') + '"]');
    $('html, body').animate({
      scrollTop: $scroll.position().top
    });
    return false;
  });
/*
  let bannerOpened = false;
  const $bannerBtn = $('.od-solution__banner');
  $(window).on('scroll', function () {
    if ($bannerBtn.offset().top < $(window).scrollTop() + $(window).height() / 2 && !bannerOpened) {
      $solution.addClass('od-solution__solution_opened');
    }
  });*/
});